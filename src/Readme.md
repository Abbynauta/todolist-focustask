## ToDoList - FocusTask

HOLA PROBANDOO

Antes de comenzar, asegurese de tener instalado OpenJDK 19.0.2 en su sistema.

├── TODOLIST
│   ├── src
│   │    └──  main
│   │        └── java
│   │             └── com
│   │                  └── login
│   │                         ├── Main.java
│   │                         │── MongoDB.java
│   │                         │── Login.java
│   │                         │── Register.java
│   │                         │── ValidateRegister.java
│   │                         │── CompletedTasks.java
│   │                         │── Task.java
│   │                         │── User.java
│   │                         │── ValidateUser.java
│   │                         │── Category.java
│   │
│   ├── resources
│   │    ├── images
│   │    └── .forms
└── README.md



___
## ToDoList - FocusTask
___
Este proyecto Utiliza la librerias JCalendar y AbsoluteLayout, mismas que se encuentran en las dependencias en el archivo pom.xml en la barra de navegacion del lado izquierdo.

# Detalles
La clase prinicipal se llama  `Main` .

En la carpeta *login* se encuentan las clases:

- `Main`:
  La clase `Main` en nuestro proyecto cumple con tres funciones principales:

1. **Conexión a la base de datos MongoDB**: Llama al método `connectToDatabase` de la clase `MongoDB` para establecer una conexión con la base de datos MongoDB, ya que nuestra aplicación trabaja con datos almacenados en la misma.

####

2. **Configuración de la Interfaz de Usuario**: Dentro del bloque `try-catch`, se encarga de configurar la apariencia visual de la interfaz gráfica de nuestra aplicación. Si surge algún error durante esta configuración, se captura en el bloque `catch` y se registra en los registros de la aplicación.
####
3. **Inicio de la Interfaz de Usuario**: Finalmente, se inicia la interfaz gráfica llamando a `new Login().setVisible(true)`. Esto crea una instancia de la clase `Login` y la hace visible. La clase `Login` es la ventana de inicio de sesión de la aplicación.

En pocas palabras, la clase `Main` es el punto de entrada de la aplicación y se encarga de establecer la conexión con la base de datos, configurar la interfaz gráfica y mostrar la ventana de inicio de sesión.

---

- `MongoDB`:
  La clase `MongoDB` tiene las siguientes responsabilidades:

1. **Conexión a la base de datos MongoDB**: La clase `MongoDB` se encarga de establecer la conexión con nuestra base de datos MongoDB utilizando la biblioteca de MongoDB para Java.

2. **Configuración de registro de MongoDB**: Configura el sistema de registro para MongoDB. Esto implica la creación de nuestro registro personalizado llamado "org.mongodb.driver" y la asociación del controlador de archivos (`FileHandler`) para registrar los eventos de MongoDB en el archivo "mongodb.log". Además, se configura el nivel de registro a `INFO`. Esto es útil para el seguimiento y la depuración de eventos relacionados con nuestra base de datos MongoDB.

3. **Mantenimiento de la conexión de la base de datos**: La clase almacena la conexión a la base de datos en la variable estática `database`, que se puede acceder mediante el método `getDatabase()`. Esto permite que otras partes de la aplicación accedan a la base de datos una vez que se ha establecido la conexión.

Por lo tanto, la clase `MongoDB` se encarga de la conexión y la configuración del registro de eventos de MongoDB. También proporciona un punto de acceso a la base de datos para otras partes de la aplicación.

---

- `Login`:
  La clase `Login` se encarga de la interfaz de inicio de sesión de la aplicación y realiza las siguientes acciones:

1. **Configuración de la interfaz gráfica**: La clase `Login` crea y configura la interfaz de inicio de sesión, incluyendo elementos como campos de entrada de texto, botones, etiquetas y eventos asociados a estos componentes.

2. **Manejo de la entrada de texto**: La clase se encarga de gestionar la entrada de texto para el nombre de usuario y la contraseña. También muestra u oculta etiquetas informativas cuando los campos de entrada están enfocados o sin enfoque.

3. **Eventos de botones y enlaces**: Define las acciones que se deben realizar cuando se hacen clic en los botones, como el botón de inicio de sesión y el botón de creación de una nueva cuenta. También maneja un enlace para recuperar la contraseña.

4. **Validación del inicio de sesión**: Cuando se hace clic en el botón de inicio de sesión, la clase verifica si el nombre de usuario y la contraseña no están vacíos. Luego, verifica si el usuario existe y si las credenciales son válidas. Si las credenciales son correctas, inicia la ventana del menú principal.

5. **Creación de una nueva cuenta**: Cuando se hace clic en el botón de creación de una nueva cuenta, se crea una instancia de la clase `Register` y se muestra para permitir a los usuarios registrar nuevas cuentas.

Resumiendo esto, la clase `Login` maneja la interfaz y la lógica de inicio de sesión de la aplicación.

---

- `Register`:
  La clase `Register` se encarga de la interfaz de registro de usuarios de la aplicación y realiza las siguientes acciones:

1. **Configuración de la interfaz gráfica**: La clase `Register` crea y configura la interfaz de registro de usuarios, incluyendo elementos como campos de entrada de texto, botones, etiquetas y eventos asociados a estos componentes.

2. **Manejo de la entrada de texto**: La clase utiliza métodos como `setupFocusListeners` para gestionar la entrada de texto en varios campos, mostrando u ocultando etiquetas informativas cuando los campos de entrada están enfocados o sin enfoque.

3. **Eventos de botones y elementos interactivos**: Define las acciones que se deben realizar cuando se hacen clic en los botones, como el botón de registro, y en el enlace para mostrar u ocultar contraseñas.

4. **Validación del registro**: Cuando se hace clic en el botón de registro, la clase recopila los datos ingresados por el usuario, como el nombre, apellido, nombre de usuario, correo electrónico y contraseña. Luego, valida estos datos según los criterios implementados, como la longitud de la contraseña y si las contraseñas coinciden. También verifica si el nombre de usuario y el correo electrónico ya existen en el sistema. Si todo es válido, crea un nuevo usuario y lo almacena en la base de datos.

5. **Mostrar u ocultar contraseñas**: La clase permite a los usuarios mostrar u ocultar las contraseñas presionando una casilla de verificación. Cuando se selecciona, muestra las contraseñas como texto sin formato, lo que permite a los usuarios verificar que han ingresado correctamente sus contraseñas.

6. **Navegación entre ventanas**: La clase permite a los usuarios volver a la ventana de inicio de sesión haciendo clic en el botón "X" en la esquina superior derecha.

Entonces, la clase `Register` maneja la interfaz y la lógica de registro de usuarios de la aplicación.

---

- `ValidateRegister`:
  Nuestra clase `ValidateUser` es la que se encarga de realizar las validaciones relacionadas con usuarios en la base de datos de MongoDB y realiza las siguientes acciones:

1. **Validación de existencia de usuario por nombre de usuario (`userExists`)**: Este método recibe un nombre de usuario como entrada y verifica si ya existe un usuario con ese nombre de usuario en la base de datos. Utiliza la colección "User" en la base de datos MongoDB y la consulta de filtrado con el nombre de usuario proporcionado. Si encuentra un usuario con el mismo nombre de usuario, devuelve `true`; de lo contrario, devuelve `false`.

2. **Validación de usuario por nombre de usuario y contraseña (`validateUser`)**: Este método recibe un nombre de usuario y una contraseña como entrada y verifica si existe un usuario con ese nombre de usuario y si la contraseña proporcionada coincide con la contraseña almacenada en la base de datos. Realiza una consulta similar a la validación anterior, pero también compara las contraseñas. Si el usuario y la contraseña coinciden, devuelve `true`; de lo contrario, devuelve `false`.

3. **Validación de existencia de usuario por correo electrónico (`emailExists`)**: Este método recibe una dirección de correo electrónico como entrada y verifica si ya existe un usuario con esa dirección de correo electrónico en la base de datos. Utiliza la colección "User" y realiza una consulta de filtrado con la dirección de correo electrónico proporcionada. Si encuentra un usuario con la misma dirección de correo electrónico, devuelve `true`; de lo contrario, devuelve `false`.

En pocas palabras la clase `ValidateUser` se utiliza para realizar comprobaciones relacionadas con la existencia de usuarios, autenticación de usuarios y existencia de correos electrónicos en la base de datos MongoDB de la app.

---

- `Menu`:
  La clase `Menu` en el proyecto es para la interfaz de usuario, para gestionar tareas y categorías en nuestra aplicación. Realiza las siguientes acciones:

1. **Inicialización de la interfaz de usuario**: En el constructor, se inicializan los componentes de la interfaz de usuario, como botones, etiquetas, tablas y campos de texto. También se configuran eventos de escucha para ciertos componentes.

2. **Métodos de manejo de eventos**:
    - `buttonEditActionPerformed`: Maneja la edición de una tarea en la tabla y actualiza la información en la base de datos MongoDB.
    - `buttonDeleteActionPerformed`: Maneja la eliminación de una tarea de la tabla y la base de datos MongoDB.
    - `tittleTaskTxtFieldActionPerformed`: Maneja eventos relacionados con el campo de texto del título de la tarea.
    - `buttonAddActionPerformed`: Maneja la creación de una nueva tarea, la cual se agrega a la tabla y se almacena en la base de datos MongoDB.
    - `validateTask`: Realiza validaciones en los campos del formulario antes de agregar una nueva tarea.
    - `buttonLogOutActionPerformed`: Maneja el cierre de sesión y regreso a la pantalla de inicio de sesión (`Login`).
    - `buttonCleanActionPerformed`: Limpia los campos del formulario.
    - `buttonCompletedActionPerformed`: Marca una tarea como completada en la base de datos MongoDB.
    - `buttonViewcompletedTasksActionPerformed`: Abre una ventana para ver las tareas completadas.

3. **Manejo de tablas**: La clase utiliza la tabla (`tableTaskDescriptionJTable`) para mostrar las tareas existentes. Cuando se realizan acciones como edición o eliminación, se actualiza la tabla y la base de datos.

4. **Conexión con MongoDB**: Se conecta a una base de datos de MongoDB para almacenar y recuperar información relacionada con tareas y categorías.

Por tanto, la clase `Menu` es la interfaz de usuario que permite a los usuarios crear, editar, eliminar y ver tareas, así como marcar tareas como completadas. También gestiona categorías relacionadas con las tareas y almacena toda esta información en la base de datos de MongoDB.

---

- `CompletedTasks`:
  La clase `CompletedTasks` funciona en la interfaz de usuario para mostrar tareas completadas en la aplicación.

1. **Inicialización de la interfaz de usuario**: En el constructor, se inicializan los componentes de la interfaz de usuario, como botones, etiquetas, tablas y el calendario. La ubicación relativa se configura para que la ventana aparezca centrada en la pantalla.

2. **Métodos de manejo de eventos**:
    - `buttonCompletedActionPerformed`: Este método maneja el botón "COMPLETED TASKS" en la parte inferior izquierda de la ventana, para mostrar tareas completadas.
    - `buttonExitCompletedTaskToMenuActionPerformed`: Maneja el botón "X" en la parte superior derecha de la ventana para volver al menú principal (`Menu`).

3. **Tabla de tareas completadas**: La clase utiliza la tabla (`table2`) para mostrar las tareas completadas.

En resumen, la clase `CompletedTasks` es una interfaz de usuario que se utiliza para ver tareas completadas.

---

- `Task`:
  La clase `Task` esta relacionada con la representación y gestión de tareas.

1. **Campos de la tarea**:
    - `expDate`: Almacena la fecha de vencimiento de la tarea.
    - `tittle`: Almacena el título o nombre de la tarea.
    - `description`: Almacena una descripción de la tarea.
    - `status`: Almacena el estado de la tarea (por defecto, "False").
    - `userId`: Almacena el identificador de usuario relacionado con la tarea.

2. **Constructor**:
    - El constructor de la clase `Task` acepta parámetros para inicializar los campos de una tarea. El estado de la tarea se establece de forma predeterminada como "False".

3. **Métodos de acceso**:
    - `getTittle`, `getDescription`, `getExpDate`: Estos métodos permiten acceder a los campos de una tarea.
    - `setTittle`, `setDescription`, `setExpDate`: Estos métodos permiten establecer los campos de una tarea.

4. **Método `toString`**:
    - Este método devuelve una representación en forma de cadena de una tarea, incluyendo el título, descripción y fecha de vencimiento.

5. **Método `toDocument`**:
    - Este método convierte una instancia de `Task` en un documento `org.bson.Document`, lo que lo hace adecuado para el almacenamiento en la base de datos MongoDB. La tarea se representa como un documento con campos "Date", "Title", "Description", "Status" y "User_Id".

6. **Método `insertTask`**:
    - Este método toma un documento de tarea y lo inserta en una colección de tareas en la base de datos MongoDB. Se utiliza para agregar tareas a la base de datos.

Por eso la clase `Task` es una representación de una tarea que se puede convertir en un documento y almacenar en nuestra base de datos MongoDB. Proporciona métodos para acceder y modificar los campos de la tarea, así como para insertar tareas en la base de datos. Esta clase es fundamental para la gestión de tareas del proyecto.

---

- `User`:
  La clase `User` en el proyecto esta relacionada con la representación y gestión de usuarios.

1. **Campos del usuario**:
    - `name`: Almacena el nombre del usuario.
    - `lastName`: Almacena el apellido del usuario.
    - `username`: Almacena el nombre de usuario (nombre de inicio de sesión).
    - `email`: Almacena la dirección de correo electrónico del usuario.
    - `password`: Almacena la contraseña del usuario.

2. **Constructor**:
    - El constructor de la clase `User` acepta parámetros para inicializar los campos de un usuario.

3. **Método `toDocument`**:
    - Este método convierte una instancia de `User` en un documento `org.bson.Document`, lo que lo hace adecuado para el almacenamiento en la base de datos MongoDB. El usuario se representa como un documento con campos "Name", "LastName", "Username", "Email" y "Password".

4. **Método `insertUser`**:
    - Este método toma un documento de usuario y lo inserta en una colección de usuarios en la base de datos MongoDB. Se utiliza para agregar usuarios a la base de datos.

En resumen, la clase `User` es una representación de un usuario que se puede convertir en un documento y almacenar en nuestra base de datos MongoDB. Proporciona métodos para convertir y almacenar usuarios en la base de datos.

---

- `ValidateUser`:
  La clase `ValidateUser` contiene métodos de validación y verificación de usuarios en el proyecto.

1. **`userExists(String username)`**:
    - Este método verifica si un usuario con un nombre de usuario específico ya existe en la base de datos. Toma el nombre de usuario como argumento y devuelve `true` si se encuentra un usuario con ese nombre de usuario en la base de datos, o `false` en caso contrario.

2. **`validateUser(String username, String password)`**:
    - Este método se utiliza para validar las credenciales de inicio de sesión de un usuario. Toma un nombre de usuario y una contraseña como argumentos y verifica si un usuario con ese nombre de usuario existe en la base de datos y si la contraseña proporcionada coincide con la contraseña almacenada para ese usuario. Devuelve `true` si las credenciales son válidas y `false` en caso contrario.

3. **`emailExists(String email)`**:
    - Este método verifica si un usuario con una dirección de correo electrónico específica ya existe en la base de datos. Toma la dirección de correo electrónico como argumento y devuelve `true` si se encuentra un usuario con esa dirección de correo electrónico en la base de datos, o `false` en caso contrario.

La clase `ValidateUser` proporciona métodos para verificar la existencia de usuarios, validar las credenciales de inicio de sesión y verificar la existencia de direcciones de correo electrónico en la base de datos. Estos métodos son fundamentales para la autenticación y gestión de usuarios en el proyecto.

---

- `Category`:
  La clase `Category` contiene métodos de gestión de categorías en nuestro proyecto.

1. **`Category(String nameCategory, String idTask)`**:
    - Este es el constructor de la clase `Category`. Toma un nombre de categoría y un ID de tarea como argumentos y establece estos valores en las variables estáticas de la clase `Category`.

2. **`getNameCategory()`**:
    - Este método estático se utiliza para obtener el nombre de la categoría actual almacenado en la variable estática `nameCategory`.

3. **`setNameCategory(String nameCategory)`**:
    - Este método estático se utiliza para establecer el nombre de la categoría en la variable estática `nameCategory`. Esto puede usarse para cambiar el nombre de la categoría.

4. **`toString()`**:
    - Este método proporciona una representación de cadena de la categoría actual y su nombre.

5. **`toDocument()`**:
    - Este método convierte la categoría en un documento BSON (JSON binario) que puede ser almacenado en la base de datos MongoDB. Retorna un objeto `Document` que representa la categoría con su nombre y el ID de la tarea asociada.

6. **`insertCategory(Document categoryDocument)`**:
    - Este método se utiliza para insertar una categoría en la base de datos. Toma un objeto `Document` que representa la categoría y la inserta en la colección "Category" de la base de datos MongoDB.

En resumen, la clase `Category` se utiliza para crear, gestionar y almacenar categorías en tu proyecto. Los métodos disponibles permiten establecer y obtener el nombre de la categoría, convertir la categoría en un documento BSON y realizar la inserción de categorías en la base de datos.

---

En la carpeta *Resources* se puede encontrar los .form y la carpeta *images* que contiene los elementos graficos que se uso en la interfaz grafica y otros.

---

## Base de Datos en Mongo
![bd1.png](main%2Fresources%2Fimages%2Fbd1.png)
![bd2.png](main%2Fresources%2Fimages%2Fbd2.png)
![bd3.png](main%2Fresources%2Fimages%2Fbd3.png)
---
# Enlace a Gitlab

---
-`< TEAM ALPHA / DATABASE 2 / 2023 />`