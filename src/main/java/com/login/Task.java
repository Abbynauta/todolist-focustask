package com.login;


import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;


import java.util.ArrayList;
import java.util.List;

import static com.login.MongoDB.getDatabase;


public class Task {
    public static String expDate;
    public static String tittle;
    public static String description;
    public static String status;
    public static String userId;

    public Task(String expDate, String tittle, String description, String userId) {
        this.tittle = tittle;
        this.description = description;
        this.expDate = expDate;
        this.status = "False";
        this.userId = userId;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String date) {
        this.expDate = date;
    }

    @Override
    public String toString() {
        return "Task{" + "tittle =" + tittle + ", description =" + description + ", date =" + expDate + '}';
    }

    public Document toDocument() {
        return new Document("Date", expDate)
                .append("Title", tittle)
                .append("Description", description)
                .append("Status", status)
                .append("User_Id", userId);
    }

    public static void insertTask(Document taskDocument) {
        MongoDatabase database = getDatabase();
        MongoCollection<Document> tasksCollection = database.getCollection("Task");
        tasksCollection.insertOne(taskDocument);
    }

    public static ArrayList<Task> getAllTasks() {
        ArrayList<Task> tasks = new ArrayList<>();

        try {
            MongoDatabase database = MongoDB.getDatabase();

            MongoCollection<Document> collection = database.getCollection("Task");


            for (Document document : collection.find().projection(new Document("Title", 1).append("Description", 1).append("Date", 1).append("User_Id", 1))) {
                String title = document.getString("Title");
                String description = document.getString("Description");
                String expirationDate = document.getString("Date");
                String userId = document.getString("User_Id");


                Task task = new Task(expirationDate, title, description, userId);
                tasks.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tasks;
    }
}