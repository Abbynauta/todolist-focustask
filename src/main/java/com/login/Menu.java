package com.login;

import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;

import static com.login.MongoDB.getDatabase;

import com.mongodb.client.model.Updates;
import com.toedter.calendar.JDateChooser;

import javax.swing.JOptionPane;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.*;
import java.text.SimpleDateFormat;

import org.bson.Document;
import org.bson.types.ObjectId;
import com.mongodb.client.model.Filters;
import com.mongodb.client.MongoCollection;

import java.text.ParseException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class Menu extends javax.swing.JFrame {
    public String expDTask, descTask, titTask, sttsTask, catTask, userId, taskId;
    public JDateChooser expirationDaterJDateChooser;
    public String nameCategory, idTask;
    public static Login login;
    public static Document filter;
    public static String newTittle;
    public static String newDescription;
    public static String newCategory;
    public static String newDate;

    private ArrayList<Task> taskList = new ArrayList<>();
    DefaultTableModel model;
    ArrayList<Category> categoryList = new ArrayList<Category>();

    public Menu() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        model = new DefaultTableModel();
        model.addColumn("TASK");
        model.addColumn("DESCRIPTION");
        model.addColumn("EXPIRATION DATE");
        model.addColumn("CATEGORY");
        tableTaskDescriptionJTable.setModel(model);


        refreshTable();

        tableTaskDescriptionJTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                int selectedRow = tableTaskDescriptionJTable.getSelectedRow();
                if (selectedRow >= 0) {
                    String tittleTask = (String) model.getValueAt(selectedRow, 0);
                    String decription = (String) model.getValueAt(selectedRow, 1);
                    String date = (String) model.getValueAt(selectedRow, 2);
                    String category = (String) model.getValueAt(selectedRow, 3);

                    tittleTaskTxtField.setText(tittleTask);
                    descriptionTaskTxtField.setText(decription);
                    categoryTxtField.setText(category);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date fechaDate = dateFormat.parse(date);
                        expirationDaterJDateChooser.setDate(fechaDate);
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }




    private void initComponents() {

        expirationDaterJDateChooser = new JDateChooser();
        backgroudWhitePanel = new javax.swing.JPanel();
        backgrounSkyBlueLeftPanel = new javax.swing.JPanel();
        buttonViewcompletedTasks = new javax.swing.JButton();
        calendarMenu = new com.toedter.calendar.JCalendar();
        dateChooserLabel = new javax.swing.JLabel();
        categoryLabel = new javax.swing.JLabel();
        tittleTaskLabel = new javax.swing.JLabel();
        descriptionTaskLabel = new javax.swing.JLabel();
        categoryTxtField = new javax.swing.JTextField();
        descriptionTaskTxtField = new javax.swing.JTextField();
        tittleTaskTxtField = new javax.swing.JTextField();
        expirationDaterJDateChooser = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableTaskDescriptionJTable = new javax.swing.JTable();
        buttonLogOut = new javax.swing.JButton();
        buttonAdd = new javax.swing.JButton();
        buttonCompleted = new javax.swing.JButton();
        buttonClean = new javax.swing.JButton();
        buttonEdit = new javax.swing.JButton();
        buttonDelete1 = new javax.swing.JButton();
        statusTask = new JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        javax.swing.JLabel logo = new javax.swing.JLabel();
        backgroudWhitePanel.setBackground(new java.awt.Color(255, 255, 255));
        backgroudWhitePanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        backgrounSkyBlueLeftPanel.setBackground(new java.awt.Color(0, 102, 255));
        backgrounSkyBlueLeftPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        logo.setIcon(new javax.swing.ImageIcon(Objects.requireNonNull(getClass().getResource("/images/focuslogo.png"))));
        backgroudWhitePanel.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));
        calendarMenu.setBackground(new java.awt.Color(0, 153, 204));
        calendarMenu.setBorder(new javax.swing.border.MatteBorder(null));
        calendarMenu.setForeground(new java.awt.Color(0, 102, 255));
        calendarMenu.setDecorationBackgroundColor(new java.awt.Color(204, 204, 204));
        calendarMenu.setWeekdayForeground(new java.awt.Color(0, 153, 0));
        backgrounSkyBlueLeftPanel.add(calendarMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 390, 420, 360));

        backgroudWhitePanel.add(backgrounSkyBlueLeftPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 420, 750));

        dateChooserLabel.setFont(new java.awt.Font("Dialog", 1, 14));
        dateChooserLabel.setForeground(new java.awt.Color(0, 0, 0));
        dateChooserLabel.setText("EXPIRATION DATE");
        backgroudWhitePanel.add(dateChooserLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 40, -1, -1));

        categoryLabel.setFont(new java.awt.Font("Dialog", 1, 14));
        categoryLabel.setForeground(new java.awt.Color(0, 0, 0));
        categoryLabel.setText("CATEGORY");
        backgroudWhitePanel.add(categoryLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 40, -1, -1));

        tittleTaskLabel.setFont(new java.awt.Font("Dialog", 1, 14));
        tittleTaskLabel.setForeground(new java.awt.Color(0, 0, 0));
        tittleTaskLabel.setText("TASK");
        backgroudWhitePanel.add(tittleTaskLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 100, -1, -1));

        descriptionTaskLabel.setFont(new java.awt.Font("Dialog", 1, 14));
        descriptionTaskLabel.setForeground(new java.awt.Color(0, 0, 0));
        descriptionTaskLabel.setText("DESCRIPTION");
        backgroudWhitePanel.add(descriptionTaskLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 160, -1, -1));

        categoryTxtField.setBackground(new java.awt.Color(229, 229, 229));
        categoryTxtField.setForeground(new java.awt.Color(0, 0, 0));
        backgroudWhitePanel.add(categoryTxtField, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 30, 200, 40));

        descriptionTaskTxtField.setBackground(new java.awt.Color(229, 229, 229));
        descriptionTaskTxtField.setForeground(new java.awt.Color(0, 0, 0));
        backgroudWhitePanel.add(descriptionTaskTxtField, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 150, 580, 40));

        tittleTaskTxtField.setBackground(new java.awt.Color(229, 229, 229));
        tittleTaskTxtField.setForeground(new java.awt.Color(0, 0, 0));
        tittleTaskTxtField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tittleTaskTxtFieldActionPerformed(evt);
            }
        });
        backgroudWhitePanel.add(tittleTaskTxtField, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 90, 580, 40));
        backgroudWhitePanel.add(expirationDaterJDateChooser, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 30, 200, 40));

        tableTaskDescriptionJTable.setBackground(new java.awt.Color(102, 153, 255));
        tableTaskDescriptionJTable.setFont(new java.awt.Font("Dialog", Font.PLAIN, 14));
        tableTaskDescriptionJTable.setForeground(new java.awt.Color(51, 51, 51));
        tableTaskDescriptionJTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                }
        ));
        jScrollPane1.setViewportView(tableTaskDescriptionJTable);

        backgroudWhitePanel.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 240, 710, 510));

        buttonLogOut.setBackground(new java.awt.Color(51, 51, 51));
        buttonLogOut.setFont(new java.awt.Font("Dialog", 0, 14));
        buttonLogOut.setForeground(new java.awt.Color(255, 255, 255));
        buttonLogOut.setText("Log Out");
        buttonLogOut.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        buttonLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLogOutActionPerformed(evt);
            }
        });
        backgroudWhitePanel.add(buttonLogOut, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 10, 100, 40));

        buttonAdd.setBackground(new java.awt.Color(0, 102, 51));
        buttonAdd.setFont(new java.awt.Font("Dialog", 0, 14));
        buttonAdd.setForeground(new java.awt.Color(255, 255, 255));
        buttonAdd.setText("Add");
        buttonAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        buttonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddActionPerformed(evt);
            }
        });
        backgroudWhitePanel.add(buttonAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 90, 100, 40));

        buttonCompleted.setBackground(new java.awt.Color(0, 102, 255));
        buttonCompleted.setForeground(new java.awt.Color(255, 255, 255));
        buttonCompleted.setText("Completed!");
        buttonCompleted.addActionListener(this::buttonCompletedActionPerformed);
        backgroudWhitePanel.add(buttonCompleted, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 700, 100, 40));

        buttonClean.setBackground(new java.awt.Color(153, 153, 153));
        buttonClean.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonClean.setForeground(new java.awt.Color(255, 255, 255));
        buttonClean.setText("Clean ");
        buttonClean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCleanActionPerformed(evt);
            }
        });
        backgroudWhitePanel.add(buttonClean, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 160, 80, 30));

        buttonEdit.setBackground(new java.awt.Color(0, 102, 102));
        buttonEdit.setFont(new java.awt.Font("Dialog", 0, 14));
        buttonEdit.setForeground(new java.awt.Color(255, 255, 255));
        buttonEdit.setText("Edit");
        buttonEdit.addActionListener(this::buttonEditActionPerformed);
        buttonEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEditActionPerformed(evt);
            }
        });
        backgroudWhitePanel.add(buttonEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 290, 100, 40));

        buttonDelete1.setBackground(new java.awt.Color(255, 51, 51));
        buttonDelete1.setFont(new java.awt.Font("Dialog", 0, 14));
        buttonDelete1.setForeground(new java.awt.Color(255, 255, 255));
        buttonDelete1.setText("DELETE");
        buttonDelete1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        buttonDelete1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });
        backgroudWhitePanel.add(buttonDelete1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 490, 100, 40));


        getContentPane().add(backgroudWhitePanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1250, 750));

        pack();
    }

    private void buttonEditActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            if (!validateTask()) {
                return;
            }

            int selectedRow = tableTaskDescriptionJTable.getSelectedRow();

            if (selectedRow >= 0) {
                newTittle = tittleTaskTxtField.getText();
                newDescription = descriptionTaskTxtField.getText();
                newCategory = categoryTxtField.getText();
                Date newDate = expirationDaterJDateChooser.getDate();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Menu.newDate = dateFormat.format(newDate);

                model.setValueAt(newTittle, selectedRow, 0);
                model.setValueAt(newDescription, selectedRow, 1);
                model.setValueAt(Menu.newDate, selectedRow, 2);
                model.setValueAt(newCategory, selectedRow, 3);

                MongoDatabase database = getDatabase();
                MongoCollection<Document> taskCollection = database.getCollection("Task");
                MongoCollection<Document> categoryCollection = database.getCollection("Category");

                Document updateCategory = new Document("$set", new Document()
                        .append("NameCategory", newCategory));

                categoryCollection.updateOne(filter, updateCategory);

                Document updating = new Document("$set", new Document()
                        .append("Date", Menu.newDate)
                        .append("Title", newTittle)
                        .append("Description", newDescription));

                taskCollection.updateOne(filter, updating);

                System.out.println("Successful Update");
            } else {
                JOptionPane.showMessageDialog(this, "Please select a row to edit",
                        "Validation Error", JOptionPane.WARNING_MESSAGE);
            }
        } catch (MongoException e) {
            System.err.println("Error UPDATE: " + e.getMessage());
            e.printStackTrace();
        }
    }


    private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableTaskDescriptionJTable.getSelectedRow();
        if (row >= 0) {
            int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this task?", "Confirm",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {
                model.removeRow(row);

                MongoDatabase database = getDatabase();
                MongoCollection<Document> taskCollection = database.getCollection("Task");

                taskCollection.deleteOne(filter);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select a row");
        }
    }


    private void tittleTaskTxtFieldActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void buttonAddActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            if (!validateTask()) {
                return;
            }

            String newTitle = tittleTaskTxtField.getText();
            String newDescription = descriptionTaskTxtField.getText();
            String newCategory = categoryTxtField.getText();

            Task task = new Task(expDTask, descTask, titTask, userId);
            task.setTittle(newTitle);
            task.setDescription(newDescription);
            taskList.add(task);

            Category category = new Category(nameCategory, idTask);
            category.setNameCategory(newCategory);
            categoryList.add(category);

            Date selectedDate = expirationDaterJDateChooser.getDate();
            if (selectedDate != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String fechaComoString = dateFormat.format(selectedDate);
                task.setExpDate(fechaComoString);
            } else {
                task.setExpDate("No seleccionado");
            }

            refreshTable();


        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "ERROR AL AGREGAR TAREA");
            e.printStackTrace();
        }

        MongoDatabase database = getDatabase();

        Date selectedDate = expirationDaterJDateChooser.getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        expirationDate = dateFormat.format(selectedDate);

        MongoCollection<Document> taskCollection = database.getCollection("User");
        Document myUser = taskCollection.find(Filters.and(
                Filters.eq("Username", Login.usernameLogin),
                Filters.eq("Password", Login.passLogin)
        )).first();

        ObjectId user = myUser.getObjectId("_id");

        expDTask = expirationDate;
        catTask = categoryTxtField.getText();
        descTask = descriptionTaskTxtField.getText();
        titTask = tittleTaskTxtField.getText();
        sttsTask = statusTask.getText();
        userId = user.toString();

        Task newTask = new Task(expDTask, titTask, descTask, userId);
        Task.insertTask(newTask.toDocument());

        MongoCollection<Document> categoryCollection = database.getCollection("Task");
        Document myTask = categoryCollection.find(Filters.and(
                Filters.eq("Title", Task.tittle),
                Filters.eq("Description", Task.description)
        )).first();
        ObjectId task = myTask.getObjectId("_id");
        taskId = task.toString();
        Category newCategory = new Category(catTask, taskId);
        Category.insertCategory(newCategory.toDocument());

        ObjectId objectId = new ObjectId(taskId);
        filter = new Document("_id", objectId);
    }


    private boolean validateTask() {
        String newTitle = tittleTaskTxtField.getText().trim();
        String newDescription = descriptionTaskTxtField.getText().trim();
        Date selectedDate = expirationDaterJDateChooser.getDate();
        String newCategory = categoryTxtField.getText().trim();

        if (newCategory.isEmpty()) {
            newCategory = "Others";
            categoryTxtField.setText(newCategory);
        }

        boolean isValid = true;

        if (newTitle.isEmpty()) {
            isValid = false;
        }

        if (newDescription.isEmpty()) {
            isValid = false;
        }

        if (selectedDate == null) {
            isValid = false;
        }

        if (!isValid) {
            JOptionPane.showMessageDialog(this, "Please fill out all fields",
                    "Validation Error", JOptionPane.WARNING_MESSAGE);
        }

        return isValid;
    }


    private void buttonLogOutActionPerformed(java.awt.event.ActionEvent evt) {
        int choice = JOptionPane.showConfirmDialog(this, "Are you sure you want to log out?", "Confirm", JOptionPane.YES_NO_OPTION);
        if (choice == JOptionPane.YES_OPTION) {
            Login login = new Login();
            login.setVisible(true);
            this.dispose();
        }
    }

    private void buttonCleanActionPerformed(java.awt.event.ActionEvent evt) {
        tittleTaskTxtField.setText("");
        descriptionTaskTxtField.setText("");
        categoryTxtField.setText("");
        expirationDaterJDateChooser.setDate(null);
    }

    private void buttonCompletedActionPerformed(java.awt.event.ActionEvent evt) {
        int selectedRow = tableTaskDescriptionJTable.getSelectedRow();

        if (selectedRow >= 0) {
            //String
            Object[] rowData = new Object[4];
            for (int i = 0; i < 4; i++) {
                rowData[i] = tableTaskDescriptionJTable.getValueAt(selectedRow, i);
            }
            /*
            CompletedTasks.addRowToTable(rowData);
*/
            String taskIndex = rowData[0].toString();

            int response = JOptionPane.showConfirmDialog(null, "Are you sure this task is completed?", "Confirm",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (response == JOptionPane.YES_OPTION) {
                MongoDatabase database = getDatabase();
                MongoCollection<Document> taskCollection = database.getCollection("Task");

                taskCollection.updateOne(
                        Filters.eq("Title", taskIndex),
                        Updates.set("Status", "true")
                );

                model.removeRow(selectedRow);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Select a row");
        }
    }




    public void refreshTable() {
        DefaultTableModel model = (DefaultTableModel) tableTaskDescriptionJTable.getModel();
        model.setRowCount(0);

        for (Task task : taskList) {
            Object[] tsk = new Object[4];
            tsk[0] = task.getTittle();
            tsk[1] = task.getDescription();
            tsk[2] = task.getExpDate();
            tsk[3] = Category.getNameCategory();
            model.addRow(tsk);
        }
        tableTaskDescriptionJTable.setModel(model);
    }

    private javax.swing.JPanel backgroudWhitePanel;
    private javax.swing.JPanel backgrounSkyBlueLeftPanel;
    private javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonClean;
    private javax.swing.JButton buttonCompleted;
    private javax.swing.JButton buttonDelete1;
    private javax.swing.JButton buttonEdit;
    private javax.swing.JButton buttonLogOut;
    private javax.swing.JButton buttonViewcompletedTasks;
    private com.toedter.calendar.JCalendar calendarMenu;
    private javax.swing.JLabel categoryLabel;
    private javax.swing.JTextField categoryTxtField;
    private javax.swing.JLabel dateChooserLabel;
    private javax.swing.JLabel descriptionTaskLabel;
    private javax.swing.JTextField descriptionTaskTxtField;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableTaskDescriptionJTable;
    private javax.swing.JLabel tittleTaskLabel;
    private javax.swing.JTextField tittleTaskTxtField;
    private String expirationDate;
    private JTextField statusTask;

}